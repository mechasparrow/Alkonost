import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

// Modal Stuff
import { ModalController} from 'ionic-angular';

//Add Feed Modal
import {FeedAdderModalComponent as FeedModal} from '../../components/feed-adder-modal/feed-adder-modal';

//Link model
import {Link} from '../../models/Link';

//Database access

import {RssDatabase} from '../../database/RssDatabase';

/**
 * Generated class for the CustomPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-custom',
  templateUrl: 'custom.html',
  providers: [RssDatabase]
})
export class CustomPage {

  public custom_links:Link[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private rss_db: RssDatabase, private modalCtrl: ModalController) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomPage');
  }

  ionViewDidEnter() {

    //Load in the current list of custom links
    let that = this;

    this.rss_db.getLinks().then (function (rss_links) {
      that.custom_links = <Link[]>rss_links;
    })


  }

  addCustomFeed() {

    let feedModal = this.modalCtrl.create(FeedModal, {})
    feedModal.present();

    let that = this;

    feedModal.onDidDismiss(function (data) {

      var link:Link = <Link>data;

      if (link != undefined) {

        if (link.link_valid()) {
          that.custom_links.push(link);
          that.rss_db.setLinks(that.custom_links);
        }

      }
    });


  }

  deleteLink(index:number) {
    this.custom_links.splice(index, 1);
    this.rss_db.setLinks(this.custom_links);
  }



}
