import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

//Import pages
import { CategoryPage } from '../category/category';
import { CustomPage} from '../custom/custom';

//Import the news feed api
import { NewsFeed } from '../../api/newsfeeds';

//Import data types

import {Link} from '../../models/Link';
import {Article} from '../../models/Article';

//Import HTTP
import { Http } from '@angular/http';

// Modal Stuff
import { ModalController, NavParams } from 'ionic-angular';

//Modals
import {FeedAdderModalComponent} from '../../components/feed-adder-modal/feed-adder-modal';

//RSS Links Database
import {RssDatabase} from '../../database/RssDatabase';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [RssDatabase]
})
export class HomePage {

  public categories:string[] = [];
  public custom_links:Link[] = [];

  constructor(public navCtrl: NavController, private http:Http, public modalCtrl: ModalController, private rss_db:RssDatabase) {

    let that = this;

    NewsFeed.getCategories(http).then (function (categories) {

      that.categories = <string[]>categories;
      console.log(that.categories);


    }).catch(function (error) {
      console.log(error);
    });

    this.rss_db.getLinks().then (function (rss_links) {
      that.custom_links = <Link[]>rss_links;
    })

  }

  categoryClick(category: string) {

    this.navCtrl.push(CategoryPage, {category: category}, {animate: false});


  }

  viewCustom() {

    this.navCtrl.push(CustomPage, {});

  }

  addRSSFeed() {

    let feedModal = this.modalCtrl.create(FeedAdderModalComponent, {})
    feedModal.present();

    let that = this;

    feedModal.onDidDismiss(function (data) {

      var link:Link = <Link>data;

      if (link != undefined) {

        if (link.link_valid()) {
          that.custom_links.push(link);
          that.rss_db.setLinks(that.custom_links);
        }

      }
    });

  }



}
