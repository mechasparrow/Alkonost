//Import the Article model
import { Article } from "../models/Article";

//Import libs for description formating
import * as cheerio from 'cheerio';

import * as truncate from 'truncate';


export class RssParse {

  static getFeed(source:string) {

    //This is the endpoint for conversion
    let rss2json = "https://alkonostfeed.glitch.me/feed"

    //This is our api promise
    var promise = new Promise(function (resolve, reject) {
      fetch(rss2json + "?url=" + source).then (function (data) {
        return data.json();
      }).then (function (json) {
        resolve(json);
      }).
      catch (function (error) {
        reject(error)
      })
    })

    return promise;

  }

  static getArticles(source:string) {

    var promise = new Promise(function (resolve, reject) {



      RssParse.getFeed(source).then (function (rss:any) {

        var Articles: Article[] = [];

        console.log(rss);

        Articles = rss.items.map(function (item) {
          return <Article> {
            title: item.title,
            author: item.author,
            content: item.content,
            description: RssParse.formatDescription(<string>item.description),
            link: item.link,
            logo: rss.meta_info.image
          }
        });

        resolve(Articles);

      }).catch (function (error) {
        reject(error)
      })

    });

    return promise;

  }

  static formatDescription(description:string) {

    var new_description:string = "";

    var contains_markup = /<[a-z][\s\S]*>/i.test(description);

    if (contains_markup == true) {
      var $ = cheerio.load(description);

      var description_text = $('p').text();

      if (description_text.length >= 140) {
        new_description = truncate(description_text, 140)
      }else {
        new_description = description_text;
      }

    }else {
      new_description = description;
    }

    return new_description;

  }

}
