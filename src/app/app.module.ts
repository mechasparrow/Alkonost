import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { CategoryPage } from '../pages/category/category';
import { CustomPage } from '../pages/custom/custom';

//Storage Module
import {IonicStorageModule} from '@ionic/storage';

//Modal Controller
import {FeedAdderModalComponent} from '../components/feed-adder-modal/feed-adder-modal';

import {HttpModule} from '@angular/http';

// For form processing
import { FormsModule }   from '@angular/forms';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CategoryPage,
    FeedAdderModalComponent,
    CustomPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CategoryPage,
    CustomPage,
    FeedAdderModalComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
